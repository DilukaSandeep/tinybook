package com.example.tinybook;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText email,password;
    private Button login,register;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.edt_login_email);
        password = (EditText) findViewById(R.id.edt_login_psw);
        login = (Button) findViewById(R.id.btn_login_login);
        register = (Button) findViewById(R.id.btn_login_register);


    }
}
