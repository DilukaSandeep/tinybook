package com.example.tinybook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button sale,purchase,account,stock,ledger,report,setting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sale = (Button) findViewById(R.id.btn_main_sales);
        purchase = (Button) findViewById(R.id.btn_main_purchase);
        account = (Button) findViewById(R.id.btn_main_account);
        stock = (Button) findViewById(R.id.btn_main_stock);
        ledger = (Button) findViewById(R.id.btn_main_ledger);
        report = (Button) findViewById(R.id.btn_main_reports);
        setting = (Button) findViewById(R.id.btn_main_setting);


        sale.setOnClickListener(this);
        purchase.setOnClickListener(this);
        account.setOnClickListener(this);
        stock.setOnClickListener(this);
        ledger.setOnClickListener(this);
        report.setOnClickListener(this);
        setting.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v == sale){
            Intent intent = new Intent(MainActivity.this,SalesActivity.class);
            startActivity(intent);
        }
        if(v == purchase){
            Intent intent = new Intent(MainActivity.this,PurchaseActivity.class);
            startActivity(intent);
        }
    }
}
